# TVMAZE TEST

It is necesary to have installed NodeJS v6+ from https://nodejs.org/es/download/ and MongoDB https://docs.mongodb.com/manual/administration/install-community/

## Dependencies

One you have all the necesary technologies, you should install all dependenies and start the code, with the next commands:

```
npm i -g nodemon
npm i
npm start
```

Then, just visit http://localhost:3000 and enjoy the experience ;) Have fun!
