function paintMovies(movies) {
  $('section.list .results').html('');

  for (var i = 0; i < movies.length; i++) {
    if(movies[i].show.image) {
      var   tmp = '<article>';
      tmp +=   '<header>';
      tmp +=     '<h3>'+movies[i].show.name+'</h3>';
      tmp +=     '<figure><img src="'+movies[i].show.image.medium+'"></figure>';
      tmp +=   '</header>';
      tmp +=   '<div class="movie-data">';
      tmp +=     movies[i].show.summary;
      tmp +=     '<div class="movie-info">';
      tmp +=       '<p><b>Rating: </b>'+movies[i].show.rating.average+'</p>';
      tmp +=       '<button class="btn btn-warning makefav" data-id="'+movies[i].show.id+'">+ Fav</button>';
      tmp +=     '</div>';
      tmp +=   '</div>';
      tmp += '</article>';

      $('section.list .results').append(tmp);
    }
  }
}

function paintVoid() {
  $('section.list .results').html('<p>No results</p>');
}

function searchMovie(e) {
  e.preventDefault();

  $.get('/movies/search/' + $('input#search').val(), function(resp){
    if (resp.movies.length > 0) return paintMovies(resp.movies);
    paintVoid();
  }, 'json');
}

function paintFavOk(target) {
  $('.alert.alert-success').addClass('open');

  $('button[data-id="'+target+'"]').remove();

  setTimeout(function(){
    $('.alert.alert-success').removeClass('open');
  }, 2000);
}

function paintFavKo() {
  $('.alert.alert-danger').addClass('open');

  setTimeout(function(){
    $('.alert.alert-danger').removeClass('open');
  }, 2000);
}


function setAsFav(e) {
  var currentID = $(e.currentTarget).attr('data-id');

  $.ajax({
    url: '/movies/fav/' + currentID,
    method: 'post',
    dataType: 'json',
    success: function (resp) {
      if (!resp.error) {
        return paintFavOk(currentID);
      }
    },
    error: function (err) {
      paintFavKo();
    }
  });
}

function leaveFav(e) {
  var currentID = $(e.currentTarget).attr('data-id');

  $.ajax({
    url: '/movies/unfav/' + currentID,
    method: 'post',
    dataType: 'json',
    success: function (resp) {
      if (!resp.error) {
        $('article#' + currentID).hide(300, () => {
          $('article#' + currentID).remove();
        });
        return paintFavOk(currentID);
      }
    },
    error: function (err) {
      paintFavKo();
    }
  });
}

$('#form-search').on('submit', searchMovie);
$(document).on('click', 'button.makefav', setAsFav);
$(document).on('click', 'button.leavefav', leaveFav);
