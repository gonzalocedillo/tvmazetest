const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const FavSchema = new Schema({
  movieId: String,
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Fav', FavSchema);
