const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');

const UserSchema = new Schema({
  username: String,
  email: String,
  password: String,
});

UserSchema.methods.createPassword = (password) => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);

  return hash;
}

UserSchema.methods.comparePassword = (password, savedPassword) => {
  return bcrypt.compareSync(password, savedPassword);
}

module.exports = mongoose.model('User', UserSchema);
