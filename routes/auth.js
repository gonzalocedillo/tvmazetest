const express = require('express');
const router = express.Router();
const UserModel = require('../models/user');


router.get('/login', function(req, res, next) {
  res.render('login');
});

router.get('/logout', function(req, res, next) {
  req.session.destroy();
  
  res.redirect('/auth/login');
});

router.post('/dologin', function(req, res, next) {
  UserModel.findOne({email: req.body.email}, (err, user) => {
    if (err) throw err;
    if (!user || !user.comparePassword(req.body.password, user.password)) {
      return res.status(400).json({error: 'user'});
    }

    req.session.user = user._id;
    req.session.username = user.username;

    return res.redirect('/app/home');
  });
});

router.get('/signin', function(req, res, next) {
  res.render('signin');
});

router.post('/dosignin', function(req, res, next) {
  UserModel.find({email: req.body.email}, (err, users) => {
    if (err) throw err;
    if (users.length > 0) return res.status(400).json({error: 'user'});

    const user = new UserModel({
      username: req.body.username,
      email: req.body.email,
    });

    user.password = user.createPassword(req.body.password);

    user.save((err, userSaved) => {
      req.session.user = userSaved._id;
      req.session.username = userSaved.username;
      // res.status(201).json({user: userSaved.username});
      return res.redirect('/app/home');
    });
  });
});

module.exports = router;
