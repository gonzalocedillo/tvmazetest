const express = require('express');
const router = express.Router();
const axios = require('axios');
const FavModel = require('../models/fav');
const myAsync = require('asyncawait/async');
const myAwait = require('asyncawait/await');

router.get('/', myAsync((req, res) => {
  const favsData = [];

  FavModel.find({userId: req.session.user}, (err, favs) => {
    const calls = [];
    for (let i = 0; i < favs.length; i++) {
      calls.push((() => axios.get('http://api.tvmaze.com/shows/' + favs[i].movieId))());
    }

    axios.all(calls)
      .then(axios.spread((...args) => {
        for (let i = 0; i < args.length; i++) {
          favsData.push(args[i].data);
        }

        return res.render('user', {favs: favsData});
      }));
  });
}));

module.exports = router;
