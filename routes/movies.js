var express = require('express');
var router = express.Router();
var axios = require('axios');
var FavModel = require('../models/fav');

router.get('/search/:movie', function(req, res, next) {
  axios.get('http://api.tvmaze.com/search/shows', {
    params: {
      q: req.params.movie
    }
  })
  .then(resp => {
    return res.status(200).json({movies: resp.data});
  })
  .catch(err => {
    return res.status(500).json({err});
  });
});

router.post('/fav/:id', (req, res) => {
  FavModel.find({userId: req.session.user, movieId: req.params.id}, (err, favs) => {
    if (err) return res.status(500).json({error: err});
    if (favs.length > 0) return res.status(400).json({error: 'duplicated'});

    const fav = new FavModel({
      userId: req.session.user,
      movieId: req.params.id,
    });

    fav.save((err, favSaved) => {
      if (err) return res.status(500).json({error: err});

      return res.status(201).json({id: favSaved._id});
    });

  });
});

router.post('/unfav/:id', (req, res) => {
  FavModel.findOne({movieId: req.params.id, userId: req.session.user}, (err, fav) => {
    if (err) res.status(500).json({error: err});

    fav.remove(err => {
      if (err) res.status(500).json({error: err});

      return res.status(200).json({deleted: true});
    });
  });
});

module.exports = router;
